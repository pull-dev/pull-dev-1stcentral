﻿using System;
using CMS.Scheduler;
using CMS.EventLog;
using CMS;
using CMS.OnlineForms;
using CMS.SiteProvider;
using CMS.DataEngine;

[assembly:RegisterCustomClass("Pull.RemoveExpiredFormDataTask", typeof(Pull.RemoveExpiredFormDataTask))]
namespace Pull
{
    /// <summary>
    /// Summary description for RemoveExpiredFormData
    /// </summary>
    public class RemoveExpiredFormDataTask: ITask
    {
        public RemoveExpiredFormDataTask()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string Execute(TaskInfo task)
        {
            string detail = "Executed from '~/App_Code/RemoveExpiredFormDataTask.cs'. Form's affected:" + task.TaskData + " With expiration time of:"+ task.TaskCondition;

            //Logs the execution of the task in the even log.
            EventLogProvider.LogInformation("RemoveExpiredFormDataTask", "Execute", detail);
            try
            {
                var condition = task.TaskCondition.Trim().Split(' ');
                int amount = 0;
                if (Int32.TryParse(condition[0], out amount))
                {
                    DateTime expire = DateTime.MinValue;
                    //based on the mentioned condition a Date time will be selected using the integer provided and the length of time identifying string.
                    if (condition[1].ToUpper().Contains("MONTH"))
                    {
                        expire = DateTime.Now.AddMonths(-amount);
                    }
                    else if (condition[1].ToUpper().Contains("YEAR"))
                    {
                        expire = DateTime.Now.AddYears(-amount);
                    }
                    else if (condition[1].ToUpper().Contains("DAY"))
                    {
                        expire = DateTime.Now.AddDays(-amount);
                    }

                    //Remove data
                    foreach (string form in task.TaskData.Split(','))
                    {
                        RemoveFormData(form.Trim(), expire);
                    }
                }
                else
                {
                    EventLogProvider.LogInformation("RemoveExpiredFormDataTask", "Failed", "Amount is not an integer.");
                    throw new Exception("");
                }
                return "Successfully run on " + DateTime.Now.ToShortDateString() + " at " + DateTime.Now.ToShortTimeString() + ". See Event Log for details.";

            }
            catch (Exception e)
            {
                if (e.Message.Length > 0)
                {
                    EventLogProvider.LogInformation("RemoveExpiredFormDataTask", "Failed", e.Message);
                }
                return "An error occured during the running of the task, please see the Event Log to see the issue.";
            }
        }

        private void RemoveFormData(string formName, DateTime expireDate)
        {
            BizFormInfo formObject = BizFormInfoProvider.GetBizFormInfo(formName, SiteContext.CurrentSiteID);
            if (formObject != null)
            {
                // Gets the class name of the form provided
                DataClassInfo formClass = DataClassInfoProvider.GetDataClassInfo(formObject.FormClassID);
                string formClassName = formClass.ClassName;

                // Loads all data records from form that are older than the provided condition
                ObjectQuery<BizFormItem> data = BizFormItemProvider.GetItems(formClassName).Where("FormInserted < '" + expireDate.ToString("yyyy-MM-dd HH:mm:ss:fff") +"'");

                // Loops through the form's data records
                foreach (BizFormItem item in data)
                {
                    // Deletes all files stored in the form's fields
                    BizFormInfoProvider.DeleteBizFormRecordFiles(formClass.ClassFormDefinition, item, SiteContext.CurrentSiteName);

                    // Deletes the form record from the database
                    item.Delete();

                    // Updates the form's number of entries (data records)
                    BizFormInfoProvider.RefreshDataCount(formObject);
                }
            }
            else
            {
                EventLogProvider.LogInformation("RemoveExpiredFormDataTask", "Failed", "The form name is incorrect. Either does not exist or there is a spelling error.");
                throw new Exception("");
            }
        }
    }
}